//
//  QuizViewController.swift
//  Fortnite_quiz
//
//  Created by Hamza on 1/29/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController {
    
       @IBOutlet weak var questionLabel: UILabel!
       @IBOutlet weak var progressBar: UIProgressView!
       @IBOutlet weak var trueButton: UIButton!
       @IBOutlet weak var falseButton: UIButton!
       var questionNumber : Int = 0
       var score = 0
       let questionArray = [
                            Questions(q: "Is karachi capital of pakistan ?", a: "False") ,
                            Questions(q: "Is tayab erdogan president of turkey ?", a: "True") ,
                            Questions(q: "2 + 2 = 4 ?", a: "True") ,
                            Questions(q: "4 * 2 = 9 ?", a: "False")
                           ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
         updateQuestions()
    }
  
    @IBAction func answerButtonPressed(_ sender: UIButton) {
        let userAnswer = sender.currentTitle
        let actualAnswer = questionArray[questionNumber].answer
        
        if userAnswer == actualAnswer {
            sender.backgroundColor = UIColor.green
            score += 1
        }
        else{
            sender.backgroundColor = UIColor.red
        }
        
        if questionNumber + 1 < questionArray.count {
         questionNumber += 1
        }
        else{
           
           performSegue(withIdentifier: "scorePage", sender: sender)
        }
        
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(updateQuestions), userInfo: nil, repeats: false)
       
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "scorePage"{
                let vc = segue.destination as! ScoreViewController
                vc.finalScore = String(score)
        }
      }
  
    @objc func updateQuestions(){
        questionLabel.text = questionArray[questionNumber].text
        trueButton.backgroundColor = UIColor.clear
        falseButton.backgroundColor = UIColor.clear
        progressBar.progress = Float(questionNumber) / Float(questionArray.count)
    }
}
