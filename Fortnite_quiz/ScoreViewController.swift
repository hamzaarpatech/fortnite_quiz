//
//  ScoreViewController.swift
//  Fortnite_quiz
//
//  Created by Hamza on 1/30/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    var finalScore : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scoreLabel.text = finalScore
    }
    
    @IBAction func restartButtonPressed(_ sender: UIButton) {
//        let main = UIStoryboard(name: "Main", bundle: nil)
//        let move = main.instantiateViewController(identifier: "homeScreen") as! ViewController
//        self.present(move, animated: true, completion: nil)
//
//        print("restart")
  
    
        
   // For going towards root controller , it will work only for roots
   // self.navigationController?.popToRootViewController(animated: true)
      
   // for check all controllers in my navigation and move to desireable any controller
   //   including root
        for controller in self.navigationController?.viewControllers ?? [] {
            if controller is ViewController {
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
}
