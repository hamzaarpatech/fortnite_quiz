//
//  HelpViewController.swift
//  Fortnite_quiz
//
//  Created by Hamza on 1/30/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {




    
    @IBOutlet weak var closeBt: UIButton!
   
    override func viewDidLoad() {
             super.viewDidLoad()
             closeBt.layer.cornerRadius = closeBt.frame.size.width / 2
             setupLabels()
             setupStackView()
             
             // fun animations
             view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapAnimations)))
         }
    
    @IBAction func closeBtPressed(_ sender: UIButton) {
        //self.navigationController?.popToRootViewController(animated: true)
       navigationController?.popViewController(animated: true)

       dismiss(animated: true, completion: nil)
    }
    
      // setup UI
        let titleLabel = UILabel()
        let bodyLabel = UILabel()
        
        fileprivate func setupLabels() {
    //        titleLabel.backgroundColor = .red
    //        bodyLabel.backgroundColor = .green
            titleLabel.numberOfLines = 1
            titleLabel.text = "Credits"
            titleLabel.font = UIFont(name: "Futura", size: 34)
            titleLabel.textColor = .white
            bodyLabel.numberOfLines = 0
            bodyLabel.text = "Hello there! If you are enjoying playing this app make sure to leave us a good review in the AppStore - Hamza Mustafa."
            bodyLabel.font = UIFont(name: "Avenir", size: 15)
            bodyLabel.textColor = .white
        }
        
        fileprivate func setupStackView() {
            let stackView = UIStackView(arrangedSubviews: [titleLabel, bodyLabel])
            stackView.axis = .vertical
            stackView.spacing = 4
        
            view.addSubview(stackView)
            
            //enables autolayout
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
           //   stackView.widthAnchor.constraint(equalTo: view.heightAnchor, constant: -100).isActive = true
            stackView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -100).isActive = true
        }
        
     
        
        @objc fileprivate func handleTapAnimations() {
            print("Animating")
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                
                self.titleLabel.transform = CGAffineTransform(translationX: -30, y: 0)
                
            }) { (_) in
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    
                    self.titleLabel.alpha = 0
                    self.titleLabel.transform = self.titleLabel.transform.translatedBy(x: 0, y: -200)
                })
                
            }
            
            UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                
                self.bodyLabel.transform = CGAffineTransform(translationX: -30, y: 0)
                
            }) { (_) in
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    
                    self.bodyLabel.alpha = 0
                    self.bodyLabel.transform = self.bodyLabel.transform.translatedBy(x: 0, y: -200)
                })
                
            }
        }
    
}
