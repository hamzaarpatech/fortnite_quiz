//
//  Questions.swift
//  Fortnite_quiz
//
//  Created by Hamza on 1/29/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation

struct Questions{
    let text : String
    let answer : String
    
    init(q:String , a:String) {
        text = q
        answer = a 
    }
}
