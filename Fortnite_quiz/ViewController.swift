//
//  ViewController.swift
//  Fortnite_quiz
//
//  Created by Hamza on 1/29/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helpButton.layer.cornerRadius = helpButton.frame.size.width / 2
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    @IBAction func callQuizController(_ sender: Any) {
         performSegue(withIdentifier: "quesPage", sender: sender)
    }
    
    
    
}

